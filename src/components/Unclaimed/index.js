import { useEffect, useState } from 'react';
import { normalizeBalance, interactWithContract } from '../../utils';
import styles from './Unclaimed.module.css';
import { TransactionConfirmationModal } from '../TransactionConfirmationModal';

let interval = null;

export const Unclaimed = ({
  contract,
  accountHash,
  setPendingTransactionHash,
  pendingTransactionHash,
}) => {
  const [unclaimed, setUnclaimed] = useState('-');
  const [confirmedTransaction, setConfirmedTransaction] = useState('');

  useEffect(() => {
    if (contract && accountHash) {
      window.clearInterval(interval);

      contract.methods.getReward(accountHash).call().then(setUnclaimed);

      interval = setInterval(() => {
        contract.methods.getReward(accountHash).call().then(setUnclaimed);
      }, 1000 * 20);
    }
  }, [contract, accountHash, pendingTransactionHash]);

  return (
    <>
      <div>Unclaimed</div>

      <div
        style={{
          fontSize: '2rem',
          fontWeight: 'bold',
          textOverflow: 'ellipsis',
          overflow: 'hidden',
        }}
        title={normalizeBalance(unclaimed)}
      >
        {normalizeBalance(unclaimed)}
      </div>

      {unclaimed > 0 && (
        <button
          disabled={pendingTransactionHash}
          className={styles.unclaimBtn}
          onClick={() => {
            interactWithContract({
              contract,
              accountAddress: accountHash,
              methodName: 'claim',
              operationName: 'send',
              onConfirmation: (confirmationNumber, receipt) => {
                setConfirmedTransaction(receipt.transactionHash);
                setPendingTransactionHash('');
              },
              onTransactionHash: (hash) => {
                setPendingTransactionHash(hash);
              },
              onError: console.error,
            });
          }}
        >
          Claim
        </button>
      )}

      <TransactionConfirmationModal
        hash={confirmedTransaction}
        onClose={() => {
          setConfirmedTransaction('');
        }}
      />
    </>
  );
};

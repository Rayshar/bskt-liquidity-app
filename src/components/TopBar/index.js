import styles from './TopBar.module.css';
import logoPath from '../../static/logo-bskt-1.png';

export const TopBar = ({ children }) => {
  return (
    <div className={styles.spaceBetween}>
      <span className={styles.logo}>
        <img src={logoPath} alt="logo" />
        <span>Liquidity</span>
      </span>

      <div className={styles.container}>
        <span className={styles.space}>{children}</span>
      </div>
    </div>
  );
};

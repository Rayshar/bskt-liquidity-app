import { useEffect, useState } from 'react';

import styles from './StakerRank.module.css';

export const StakerRank = ({
  contract,
  accountHash,
  pendingTransactionHash,
}) => {
  const [rank, setRank] = useState('-');
  const [stake, setStake] = useState(0);

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .getStakedAmount(accountHash)
        .call()
        .then(setStake)
        .catch((error) => {
          console.error('[ERROR: Staked (getStakedAmount)]:', error);
        });

      if (stake > 0) {
        contract.methods
          .getStakedRank(accountHash)
          .call()
          .then((receipt) => {
            setRank(receipt);
          });
      }
    }
  }, [accountHash, contract, pendingTransactionHash, stake]);

  return (
    <>
      <span>Staker Rank</span>
      <strong className={styles.rank}>{rank}</strong>
    </>
  );
};

import styles from './Modal.module.css';
import { Button } from '../Button';

export const Prompt = ({
  show,
  title,
  onClose = () => {},
  onConfirm = () => {},
  onCancel = () => {},
  children,
}) => {
  /**
   * onModalClose
   */
  const onModalClose = () => {
    onClose();
  };

  return (
    <>
      <div className={`${styles.modal} ${show ? styles.show : ''}`}>
        <span className={styles.close} onClick={onModalClose}>
          &times;
        </span>

        {title && <div className={styles.title}>{title}</div>}

        <div className={styles.body}>{children}</div>

        <div className={styles.buttonContainer}>
          <Button
            onClick={() => {
              onCancel();
            }}
          >
            Cancel
          </Button>

          <Button
            className={styles.buttonConfirm}
            onClick={() => {
              onConfirm();
            }}
          >
            Confirm
          </Button>
        </div>
      </div>
    </>
  );
};

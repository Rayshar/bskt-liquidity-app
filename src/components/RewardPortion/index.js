import { useEffect, useState } from 'react';

import styles from './RewardPortion.module.css';

export const RewardPortion = ({
  contract,
  accountHash,
  pendingTransactionHash,
}) => {
  const [rewardPortion, setRewardPortion] = useState('-');

  useEffect(() => {
    if (contract && accountHash) {
      contract.methods
        .getRewardPortion()
        .call()
        .then((receipt) => {
          setRewardPortion(receipt);
        });
    }
  }, [accountHash, contract, pendingTransactionHash]);

  return (
    <>
      <span>Reward Portion</span>
      <strong className={styles.portion}>
        {rewardPortion !== '-' ? `${rewardPortion}%` : rewardPortion}
      </strong>
    </>
  );
};

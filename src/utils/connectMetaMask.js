import { isMetaMaskInstalled } from '.';

export const connectMetaMask = () => {
  if (isMetaMaskInstalled()) {
    return window.ethereum.request({ method: 'eth_requestAccounts' });
  }

  alert(
    'To connect to our staking platform please install and unlock MetaMask wallet',
  );
};

import { etherScanApiKey } from '../constants';

export const estimateGasPrice = () => {
  return window.fetch(
    `https://api.etherscan.io/api?module=gastracker&action=gasoracle&apikey=${etherScanApiKey}`,
  );
};

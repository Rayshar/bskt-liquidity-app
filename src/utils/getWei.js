import web3 from 'web3';

export const getWei = (value) => {
  return web3.utils.toWei(value.toString(), 'ether');
};

import { isMetaMaskInstalled } from './isMetaMaskInstalled';

export const isMetaMaskLocked = () => {
  return new window.Promise((resolve, reject) => {
    if (isMetaMaskInstalled()) {
      window.ethereum
        .request({ method: 'eth_requestAccounts' })
        .then((accounts) => {
          if (accounts.length === 0) {
            console.info('MetaMask is locked');
            resolve(accounts);
            return;
          }

          console.info('MetaMast is unlocked');
          resolve([]);
        })
        .catch((error) => {
          if (error.code === 4001) {
            alert('Please connect to MetaMask.');
          }

          if (error !== null) {
            console.error(
              '[ERROR]: .request({ method: "eth_requestAccounts" })',
              error,
            );
          }

          reject([]);
        });
    }

    reject();
  });
};

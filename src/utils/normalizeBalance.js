import { coinDecimalPlaces } from '../constants';

export const normalizeBalance = (balance = '-') => {
  if (balance.length > 1) {
    const balanceDecimalized = balance / Number(1 + `e+${coinDecimalPlaces}`);

    return balanceDecimalized > 1000
      ? balanceDecimalized.toFixed(0)
      : balanceDecimalized.toFixed(4);
  }

  return balance;
};

import { convertGweiToWei } from './convertGweiToWei';
import { estimateGasPrice } from './estimateGasPrice';
import { gas } from '../constants';

export const interactWithContract = ({
  contract,
  accountAddress,
  methodName,
  methodValue,
  operationName,
  operationParams = {},
  onTransactionHash,
  onConfirmation,
  onError,
}) => {
  if (contract && accountAddress) {
    estimateGasPrice()
      .then((data) => data.json())
      .then((result) => {
        const gasPriceInWei = convertGweiToWei(result.fast / 10);

        if (methodValue) {
          contract.methods[methodName](methodValue)
            [operationName]({
              ...operationParams,
              from: accountAddress,
              gas,
              gasPrice: gasPriceInWei,
            })
            .on('transactionHash', (hash) => {
              console.log('transactionHash', hash);

              onTransactionHash(hash);
            })
            .on('confirmation', (confirmationNumber, receipt) => {
              console.log('confirmationNumber', confirmationNumber);
              console.log('receipt', receipt);

              if (confirmationNumber === 1) {
                onConfirmation(confirmationNumber, receipt);
              }
            })
            .on('error', onError);
        } else {
          contract.methods[methodName]()
            [operationName]({
              ...operationParams,
              from: accountAddress,
              gas,
              gasPrice: gasPriceInWei,
            })
            .on('transactionHash', (hash) => {
              console.log('transactionHash', hash);

              onTransactionHash(hash);
            })
            .on('confirmation', (confirmationNumber, receipt) => {
              console.log('confirmationNumber', confirmationNumber);
              console.log('receipt', receipt);

              if (confirmationNumber === 1) {
                onConfirmation(confirmationNumber, receipt);
              }
            })
            .on('error', onError);
        }
      });
  }
};
